package com.ADDocean.app.model;

import com.fazecast.jSerialComm.SerialPort;

public class SerialCommunication {

    private SerialPort serial;
    private byte[] InputBuffer;
    private  ControllerCom controllerCom;

    public SerialCommunication(ControllerCom controller) {
        controllerCom = controller;
    }

    public void InitPort(String comport,int baudrate,int databits,int stopbits,int parity,int readtimeout,int writetimeout)
    {
        //SerialPort serial = SerialPort.getCommPorts()[0];
        serial.getCommPort(comport);
        serial.openPort();
        serial.setComPortParameters(baudrate, databits, stopbits, parity);
        serial.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, readtimeout, writetimeout);

    }

    public void WriteMessage(byte[] message)
    {
        serial.writeBytes(message,message.length);
    }

    public void ReadMessage()
    {
        serial.readBytes(InputBuffer,InputBuffer.length);
    }
}
