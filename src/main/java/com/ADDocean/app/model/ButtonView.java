package com.ADDocean.app.model;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
public class ButtonView {

    private int outputsNumber = 48;

    // Digital Outputs
    private String[] selectedOutputs={};
    private List<String> outputs;

    // Analog Outputs
    private float SA1,SA2,SA3,SA4,SA5,SA6,SA7,SA8,SA9,SA10,SA11,SA12,SA13,SA14,SA15,SA16;

    // Displays
    private byte display1,display2,display3,display4,display5,display6,display7,display8,display9,display10,
            display11,display12,display13,display14,display15,display16;

    // Analog Inputs
    private float value1,value2,value3,value4,value5,value6,value7,value8;

    // Puntero al controlador
    private ControllerCom controllerCom;

    public ButtonView(){}

    @PostConstruct
    public void init()
    {
        outputs = new ArrayList<String>();

        for(int i = 0; i < outputsNumber; i++)
        {
            outputs.add("SD" + (i+1));
        }

        controllerCom = new ControllerCom (this);
    }
    public void setSelectedOutputs(String[] selectedOutputs)
    {
        this.selectedOutputs = selectedOutputs;
    }

    public String[] getSelectedOutputs()
    {
        return selectedOutputs;
    }

    public List<String> getOutputs()
    {
        return outputs;
    }

    ////// Analog Outputs //////////////////
    public float getSA1()
    {
        return SA1;
    }

    public void setSA1(float SA1)
    {
        this.SA1 = SA1;
    }

    public float getSA2() {
        return SA2;
    }

    public void setSA2(float SA2) {
        this.SA2 = SA2;
    }

    public float getSA3() {
        return SA3;
    }

    public void setSA3(float SA3) {
        this.SA3 = SA3;
    }

    public float getSA4() {
        return SA4;
    }

    public void setSA4(float SA4) {
        this.SA4 = SA4;
    }

    public float getSA5() {
        return SA5;
    }

    public void setSA5(float SA5) {
        this.SA5 = SA5;
    }

    public float getSA6() {
        return SA6;
    }

    public void setSA6(float SA6) {
        this.SA6 = SA6;
    }

    public float getSA7() {
        return SA7;
    }

    public void setSA7(float SA7) {
        this.SA7 = SA7;
    }

    public float getSA8() {
        return SA8;
    }

    public void setSA8(float SA8) {
        this.SA8 = SA8;
    }

    public float getSA9() {
        return SA9;
    }

    public void setSA9(float SA9) {
        this.SA9 = SA9;
    }

    public float getSA10() {
        return SA10;
    }

    public void setSA10(float SA10) {
        this.SA10 = SA10;
    }

    public float getSA11() {
        return SA11;
    }

    public void setSA11(float SA11) {
        this.SA11 = SA11;
    }

    public float getSA12() {
        return SA12;
    }

    public void setSA12(float SA12) {
        this.SA12 = SA12;
    }

    public float getSA13() {
        return SA13;
    }

    public void setSA13(float SA13) {
        this.SA13 = SA13;
    }

    public float getSA14() {
        return SA14;
    }

    public void setSA14(float SA14) {
        this.SA14 = SA14;
    }

    public float getSA15() {
        return SA15;
    }

    public void setSA15(float SA15) {
        this.SA15 = SA15;
    }

    public float getSA16() {
        return SA16;
    }

    public void setSA16(float SA16) {
        this.SA16 = SA16;
    }

    ////// Analog Inputs////////////////////
    public float getValue1()
    {
        return value1;
    }

    public void setValue1(float value)
    {
        this.value1 = value;
    }

    public float getValue2()
    {
        return value2;
    }

    public void setValue2(float value)
    {
        this.value2 = value;
    }

    public float getValue3()
    {
        return value3;
    }

    public void setValue3(float value)
    {
        this.value3 = value;
    }

    public float getValue4()
    {
        return value4;
    }

    public void setValue4(float value)
    {
        this.value4 = value;
    }

    public float getValue5()
    {
        return value5;
    }

    public void setValue5(float value)
    {
        this.value5 = value;
    }

    public float getValue6()
    {
        return value6;
    }

    public void setValue6(float value)
    {
        this.value6 = value;
    }

    public float getValue7()
    {
        return value7;
    }

    public void setValue7(float value)
    {
        this.value7 = value;
    }

    public float getValue8()
    {
        return value8;
    }

    public void setValue8(float value)
    {
        this.value8 = value;
    }

    ////// Displays //////
    public  void  setDisplay1(byte display1)
    {
        this.display1 = display1;
    }

    public byte getDisplay1()
    {
        return display1;
    }

    public  void  setDisplay2(byte display2)
    {
        this.display2 = display2;
    }

    public byte getDisplay2()
    {
        return display2;
    }

    public  void  setDisplay3(byte display3)
    {
        this.display3 = display3;
    }

    public byte getDisplay3()
    {
        return display3;
    }

    public  void  setDisplay4(byte display4)
    {
        this.display4 = display4;
    }

    public byte getDisplay4()
    {
        return display4;
    }

    public  void  setDisplay5(byte display5)
    {
        this.display5 = display5;
    }

    public byte getDisplay5()
    {
        return display5;
    }

    public  void  setDisplay6(byte display6)
    {
        this.display6 = display6;
    }

    public byte getDisplay6()
    {
        return display6;
    }

    public  void  setDisplay7(byte display7)
    {
        this.display7 = display7;
    }

    public byte getDisplay7()
    {
        return display7;
    }

    public  void  setDisplay8(byte display8)
    {
        this.display8 = display8;
    }

    public byte getDisplay8()
    {
        return display8;
    }

    public  void  setDisplay9(byte display9)
    {
        this.display9 = display9;
    }

    public byte getDisplay9()
    {
        return display9;
    }

    public  void  setDisplay10(byte display10)
    {
        this.display10 = display10;
    }

    public byte getDisplay10()
    {
        return display10;
    }

    public  void  setDisplay11(byte display11)
    {
        this.display11 = display11;
    }

    public byte getDisplay11()
    {
        return display11;
    }

    public  void  setDisplay12(byte display12)
    {
        this.display12 = display12;
    }

    public byte getDisplay12()
    {
        return display12;
    }

    public  void  setDisplay13(byte display13)
    {
        this.display13 = display13;
    }

    public byte getDisplay13()
    {
        return display13;
    }

    public  void  setDisplay14(byte display14)
    {
        this.display14 = display14;
    }

    public byte getDisplay14()
    {
        return display14;
    }

    public  void  setDisplay15(byte display15)
    {
        this.display15 = display15;
    }

    public byte getDisplay15()
    {
        return display15;
    }

    public  void  setDisplay16(byte display16)
    {
        this.display16 = display16;
    }

    public byte getDisplay16()
    {
        return display16;
    }

    public void captureOutput()
    {
        controllerCom.captureOutput();
    }

    public void printMessage(String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
    }
}