package com.ADDocean.app.model;

import javax.faces.context.FacesContext;

public class ControllerCom {

    // Objeto que controla la vista
    private ButtonView button;

    // Controlador del puerto serie
    private SerialCommunication serialPortController;

    private String[] lastOutputs = {};

    public ControllerCom(ButtonView buttonView) {
        button = buttonView;

        serialPortController = new SerialCommunication(this);
    }

    public void captureOutput()
    {
        String[] selectedOutputs = button.getSelectedOutputs();

        String checkedOutput = "";
        String uncheckedOutput = "";

        for (int i = 0; i < selectedOutputs.length; i++)
        {
            checkedOutput = selectedOutputs[i];
            for (int j = 0; j < lastOutputs.length; j++)
            {
                if (selectedOutputs[i].equals(lastOutputs[j]))
                {
                    checkedOutput = "";
                    break;
                }
            }
            if (!checkedOutput.isEmpty())
            {
                break;
            }
        }
        if (!checkedOutput.isEmpty())
        {
            addOutputMessage(true, checkedOutput);
        }

        for (int i = 0; i < lastOutputs.length; i++)
        {
            uncheckedOutput = lastOutputs[i];
            for (int j = 0; j < selectedOutputs.length; j++)
            {
                if (lastOutputs[i].equals(selectedOutputs[j]))
                {
                    uncheckedOutput = "";
                    break;
                }
            }
            if (!uncheckedOutput.isEmpty())
            {
                break;
            }
        }

        if (!uncheckedOutput.isEmpty())
        {
            addOutputMessage(false, uncheckedOutput);
        }

        lastOutputs = selectedOutputs;
    }

    public void addOutputMessage(boolean checked, String element) {

        String message = "Selected Digital Output: ";

        if (!checked)
        {
            message = "Unselected Digital Output: ";
        }

        button.printMessage(message + element);

    }
}